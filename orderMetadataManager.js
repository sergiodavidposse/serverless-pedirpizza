"use strict";

const AWS = require("aws-sdk");
const dynamo = new AWS.DynamoDB.DocumentClient();

const COMPLETED_ORDER_TABLE_NAME = process.env.COMPLETED_ORDER_TABLE;


module.exports.saveCompletedOrder = async (order) => {
  try {
    console.log("saveCompletedOrder was called");
    order.delivery_status = "READY_FOR_DELIVERY";
    const params = {
      TableName: COMPLETED_ORDER_TABLE_NAME,
      Item: order,
    };
    return dynamo
      .put(params)
      .promise();
  } catch (err) {
    console.log("error => ", err);
  }
};

module.exports.deliverOrder = orderId => {
  console.log("deliverOrder was called", orderId);
  const params = {
    TableName: COMPLETED_ORDER_TABLE_NAME,
    Key: {
      orderId,
    },
    ConditionExpression: "attribute_exists(orderId)",
    UpdateExpression: "set delivery_status = :v",
    ExpressionAttributeValues: {
      ":v": "DELIVERED",
    },
    ReturnValues: "ALL_NEW",
  };
  return dynamo
    .update(params)
    .promise()
    .then((response) => {
      console.log("order delivered");
      return response.Attributes;
    });
};
