"use strict";
const { v1 } = require("uuid");
const AWS = require("aws-sdk");
const orderMetadataManager = require("./orderMetadataManager");

var sqs = new AWS.SQS({ region: process.env.REGION });
const QUEUE_URL = process.env.PENDING_ORDER_QUEUE;

module.exports.makeOrder = async (event, _context, callback) => {
  const orderId = v1();
  const body = JSON.parse(event.body);
  const order = {
    orderId: orderId,
    name: body.name,
    address: body.address,
    pizzas: body.pizzas,
    timestamp: Date.now(),
  };
  const params = {
    MessageBody: JSON.stringify(order),
    QueueUrl: QUEUE_URL,
  };
  try {
    const result = await sqs.sendMessage(params).promise();
    const message = {
      messageId: result.MessageId,
      orderId: orderId,
    };
    sendResponse(200, message, callback);
  } catch (error) {
    sendResponse(500, error, callback);
  }
};

module.exports.prepareOrder = async (event, _context, callback) => {
  console.log("prepareOrder was called");
  let order = { orderId: "0" };
  if (event.Records) {
    order = JSON.parse(event.Records[0].body);
  }
  await orderMetadataManager.saveCompletedOrder(order);
  callback();
};

module.exports.deliverOrder = async (event, _context, callback) => {
  if (event.Records) {
    const record = event.Records[0];
    if (record.eventName === "INSERT") {
      const orderId = record.dynamodb.Keys.orderId.S;
      await orderMetadataManager.deliverOrder(orderId);
      callback();
    }
    console.log(event);
    callback();
  } else {
    console.log("Is not a new record");
    callback();
  }
};

function sendResponse(statusCode, message, callback) {
  const response = {
    statusCode,
    body: JSON.stringify(message),
  };
  return callback(null, response);
}

